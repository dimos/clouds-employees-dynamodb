import React, { useState, useRef, useEffect } from "react";
import EmployeeList from './EmployeeList';
// import { v4 as uuid_v4 } from "uuid";

const API_URL = 'https://1q8x6jao8k.execute-api.us-east-2.amazonaws.com'


function App() {
  const [employees, setEmployees] = useState([])
  // const textRef = useRef()
  const nameRef = useRef()
  const idRef = useRef()
  const salaryRef = useRef()
  const workinghoursRef = useRef()

  function handleGetEmployees(e) {
    fetch(API_URL+'/employees').then((response) => response.json())
          .then(function(data) {
            // console.log(data.Items)
            setEmployees(data.Items)
          })
          .catch((error) => console.log(error));
    // textRef.current.value = null
  }

  useEffect(() => {
    handleGetEmployees()
  }, [])

  // function handleGetEmployeeById(e) {
  //   // const text = textRef.current.value
  //   // if (text === "") return
  //   // console.log(API_URL+'/employees/'+text)
  //   fetch(API_URL+'/employees/'+text).then((response) => response.json())
  //   .then(function(data) {
  //     if (data.Item){
  //       // console.log(data.Item)
  //       // console.log({id: '1', name: "Hogn Colton"})
  //       setEmployees([data.Item])
  //     }
  //     else {
  //       setEmployees([])
  //     }
  //   })
  //   .catch((error) => console.log(error));
  // }

  function handlePutEmployees(e) {
    const name = nameRef.current.value
    const id = idRef.current.value
    const salary = salaryRef.current.value
    const workinghours = workinghoursRef.current.value
    console.log(name, id, salary, workinghours)
    // const text = textRef.current.value
    // if (text === "") return
    if (name === "" || id === "" || salary === "" || workinghours === "") return
    fetch(API_URL+'/employees', {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({id: id, name: name, salary: salary, workinghours: workinghours})
    }).then((response) => response.json())
    .then(function(data) {
      // textRef.current.value = data
      handleGetEmployees()
    })
    .catch((error) => console.log(error));
  }

  function handleDeleteEmployeeById(id) {
    const text = id
    if (text === "") return
    fetch(API_URL+'/employees/'+text, {
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/json',
      },
      body: text
    }).then((response) => response.json())
    .then(function(data) {
      // textRef.current.value = data
      handleGetEmployees()
    })
    .catch((error) => console.log(error));
  }

  return (
    <>
      <EmployeeList
      employees={employees}
      handleDeleteEmployeeById={handleDeleteEmployeeById}
      handlePutEmployees={handlePutEmployees}
      nameRef={nameRef}
      idRef={idRef}
      salaryRef={salaryRef}
      workinghoursRef={workinghoursRef}
      />
      <ul>
        {/* <input ref={todoNameRef} type="text" />
        <div>{employees.filter(todo => !todo.complete).length} left todo</div>
        <button onClick={handleGetEmployeeById}>GET Employee with ID</button>
        ID Required
        <br></br>
        <button onClick={handlePutEmployees}>PUT Employees</button>
        Raw JSON Required <br></br> Example: 	&#123;"id":"2","name":"William Harbors","salary":"20000","workinghours":"138833"&#125;
        
        <br></br>
        <br></br>
        <h5>Text Input:</h5>
        <input ref={textRef} type="text" size="80" /> */}
      </ul>
    </>
  )
}

export default App;
