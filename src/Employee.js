import React from "react";

export default function Employee({employee}) {   
    // console.log(employee)
    return(
        <div>
            <h3>
                {employee.name}
            </h3>
            <label>
                <ul>
                ID: {employee.id}
                <br></br>
                Working Hours: {employee.workinghours}
                <br></br>
                Salary: {employee.salary}
                </ul>
            </label>
        </div>
    )
}