import React from "react";
// import Employee from './Employee'
import { v4 as uuid_v4 } from "uuid";

export default function EmployeeList({employees, handleDeleteEmployeeById, handlePutEmployees, nameRef, idRef, salaryRef, workinghoursRef}) {

    function handlePut(e) {
        handlePutEmployees()
    }

    if (employees.length === 0) return (<h2>LOADING</h2>)
    return (
        <>
        <table>
            <thead>
                <tr>
                    <th>Name</th>
                    <th>ID</th>   
                    <th>Salary</th>
                    <th>Working Hours</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                {
                employees.map(employee => {

                    function handleDelete(e) {
                        handleDeleteEmployeeById(employee.id)
                    }

                    return (<tr key={uuid_v4()}>
                        <td>{employee.name}</td>
                        <td>{employee.id}</td>
                        <td>{employee.salary}</td>
                        <td>{employee.workinghours}</td>
                        <td><button onClick={handleDelete}>DELETE</button></td>
                    </tr>)
                })
                }
                <tr>
                    <td><input ref={nameRef} type="text"/></td>
                    <td><input ref={idRef} type="text"/></td>
                    <td><input ref={salaryRef} type="text"/></td>
                    <td><input ref={workinghoursRef} type="text"/></td>
                    <th><button onClick={handlePut}>INSERT/UPDATE</button></th>
                </tr>
            </tbody>
        </table>
        </>
    )
}